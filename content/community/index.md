---
title: "Community"
date: 2021-10-06T10:00:00-04:00
footer_class: "footer-darker"
hide_sidebar: true
layout: "single"
---

{{< grid/section-container class="featured-news margin-top-40">}}
  {{< grid/div class="row" isMarkdown="false" >}}
    {{< grid/div class="col-sm-12" isMarkdown="false" >}}
      {{< newsroom/news
          title="News"
          id="news-list-container"
          publishTarget="oniro"
          count="5"
          class="col-sm-24"
          templateId="custom-news-template" templatePath="/js/templates/news-home.mustache"
          paginate="true" >}}
    {{</ grid/div >}}

    {{< grid/div class="col-sm-12 featured-events text-center" isMarkdown="false" >}}
      {{< newsroom/events
          title="Upcoming Events"
          publishTarget="oniro"
          class="col-sm-24"
          containerClass="event-timeline"
          upcoming="1"
          templateId="custom-events-template" templatePath="js/templates/event-list-format.mustache"
          count="4"  includeList="true" >}}
    {{</ grid/div >}}
   {{</ grid/div >}}
{{</ grid/section-container >}}